source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.3'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
gem 'redis-rails'

# Webpacker makes it easy to use the JavaScript pre-processor and bundler
gem 'webpacker', '~> 3.5'

# Haml as the templating engine
gem 'hamlit'
gem 'hamlit-rails'

# Draper adds an object-oriented layer of presentation logic to your Rails application.
gem 'draper'

# Catch unsafe migrations at dev time
gem 'strong_migrations'

# ActiveModelSerializers brings convention over configuration to your JSON generation
gem 'active_model_serializers', '~> 0.10.0'

# DSL and Rails engine for documenting your RESTful API
gem 'apipie-rails'

# Devise is a flexible authentication solution for Rails based on Warden
gem 'devise'
gem 'jwt'

# A Scope & Engine based, clean, powerful, customizable and sophisticated paginator
gem 'kaminari'

# Simple and extremely flexible way to upload files from Ruby applications
gem 'carrierwave', '~> 1.0'
gem 'cloudinary'

# Simple efficient background processing for Ruby.
gem 'sidekiq'

# Interactor provides a common interface for performing complex user interactions.
gem 'interactor', '~> 3.0'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Use Capistrano for deployment to dev/staging
  # gem 'capistrano'
  # gem 'capistrano-rails'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end
