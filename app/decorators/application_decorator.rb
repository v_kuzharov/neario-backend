class ApplicationDecorator < Draper::Decorator
  # Define methods for all decorated objects.
  # Helpers are accessed through `helpers` (aka `h`).

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def created_at_formated
    object.created_at.strftime('%d/%m/%Y')
  end

  def current_admin
    h.current_admin
  end
end