module ApplicationHelper
  def flash_messages!
    output = flash.map do |type, massage|
      key = case type
            when 'notice'
              'success'
            when 'alert'
              'danger'
            else
              type
            end
      content_tag(:div, id: 'flash-messages' ) do
        content_tag(:div, class: "alert alert-#{key} alert-dismissible fade show", role: 'alert') do
          concat(massage)
        end
      end
    end.join
    raw(output)
  end

  def markup_for_admin
    'app header-fixed sidebar-fixed sidebar-lg-show' if admin_signed_in?
  end

  def current_page_header(path)
    path.split('/').last.capitalize
  end

  def active_class(link_path)
    current_page?(link_path) ? 'active' : ''
  end
end
