import Rails from "rails-ujs";
Rails.start();

import '@coreui/coreui';

import '../init/admin/deferred.scss'

setTimeout(() => {
    $("#flash-messages").fadeTo(500, 0).slideUp(500, function() {
        return $(this).remove();
    });
}, 2000);