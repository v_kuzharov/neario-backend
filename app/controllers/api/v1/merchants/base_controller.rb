module Api
  module V1
    class Merchants::BaseController < Api::ApiBaseController
      before_action :authenticate_merchant!
      before_action :detect_spy!



      private

      def detect_spy!
        if current_merchant && current_merchant.class.to_s.downcase != 'merchant'
          throw_error!(status: :unauthorized, details: 'Restricted area!')
        end
      end
    end
  end
end