module Api
  module V1
    class Merchants::AuthController < Merchants::BaseController
      extend Controllers::ApiDoc
      expose_doc

      skip_before_action :authenticate_merchant!, only: [:register, :login]


      def login
        result = Merchant::Login.call!(login_params)

        success! auth_token: result.token,
                 merchant: MerchantSerializer.new(result.merchant)
      rescue Interactor::Failure => e
        throw_error!(details: e.context.errors)
      end

      def register
        result = Merchant::Register.call!(register_params)

        success! auth_token: result.token,
                 merchant: MerchantSerializer.new(result.merchant)
      rescue Interactor::Failure => e
        throw_error!(details: e.context.errors)
      end

      def logout
        current_merchant.update!(jti: nil)

        render json: {message: 'Sign out successfully!'}, status: :ok
      rescue => e
        throw_error!(status: :unprocessable_entity, details: e.message)
      end



      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def register_params
        params.require(:merchant).permit(:email, :store_name, :store_email, :password, :password_confirmation, :phone, :address, :lat, :lng)
      end

      def login_params
        params.require(:merchant).permit(:email, :password)
      end
    end
  end
end