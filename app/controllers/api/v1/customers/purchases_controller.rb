module Api
  module V1
    class Customers::PurchasesController < Customers::BaseController
      extend Controllers::ApiDoc
      expose_doc


      def create
        result = Operation::MakePurchase.call!(purchase_params.merge(customer: current_customer) )

        success! purchase: PurchaseSerializer.new(result.purchase)
      rescue Interactor::Failure => e
        throw_error!(status: e.context.error_status, details: e.context.errors)
      end

      def history
        success! purchases: Purchase.where(customer_id: current_customer.id)
                                    .map { |p| PurchaseSerializer.new(p)}
      end



      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def purchase_params
        params.require(:purchase).permit(:deal_id, :merchant_id)
      end
    end
  end
end