module Api
  module V1
    class Customers::AuthController < Customers::BaseController
      extend Controllers::ApiDoc
      expose_doc

      skip_before_action :authenticate_customer!, only: [:register, :login]


      def login
        result = Customer::Login.call!(login_params)

        success! auth_token: result.token,
                 customer: CustomerSerializer.new(result.customer)
      rescue Interactor::Failure => e
        throw_error!(details: e.context.errors)
      end

      def register
        result = Customer::Register.call!(register_params)

        success! auth_token: result.token,
                 customer: CustomerSerializer.new(result.customer)
      rescue Interactor::Failure => e
        throw_error!(details: e.context.errors)
      end

      def logout
        current_customer.update!(jti: nil)

        render json: {message: 'Sign out successfully!'}, status: :ok
      rescue => e
        throw_error!(status: :unprocessable_entity, details: e.message)
      end



      private

      # Never trust parameters from the scary internet, only allow the white list through.
      def register_params
        params.require(:customer).permit(:email, :first_name, :password, :password_confirmation, :last_name, :mailing)
      end

      def login_params
        params.require(:customer).permit(:email, :password)
      end
    end
  end
end