module Api
  module V1
    class Customers::BaseController < Api::ApiBaseController
      before_action :authenticate_customer!
      before_action :detect_spy!



      private

      def detect_spy!
        if current_customer && current_customer.class.to_s.downcase != 'customer'
          throw_error!(status: :unauthorized, details: 'Restricted area!')
        end
      end
    end
  end
end