module Api
  module V1
    class Customers::CategoriesController < Customers::BaseController
      extend Controllers::ApiDoc
      expose_doc

      skip_before_action :authenticate_customer!

      def index
        success! categories: Category.all
                                     .map { |cat| CategorySerializer.new(cat)}
      end

      def category_deals
        category = Category.find(params[:category_id])

        # Should be refactored in future!
        # Show all deals for 'All' category only in angular coordinates!
        category_deals = category.name.eql?('All') ? Deal.all : category.deals

        success! deals: category_deals.includes(:merchant)
                                      .map { |deal| DealSerializer.new(deal) }
      rescue ActiveRecord::RecordNotFound => e
        throw_error!(status: :not_found, details: e.message)
      end
    end
  end
end