module Api
  class ApiBaseController < ApplicationController

    private

    def success!(**attr)
      render json: {data: attr}, status: :ok
    end

    def throw_error!(status: :bad_request, details:)
      render json: {errors: ErrorsSerializer.new(nil, d: details)}, status: status
    end
  end
end