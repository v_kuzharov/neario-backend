class ApplicationController < ActionController::Base
  protect_from_forgery if: :json_request # return null session when API call
  protect_from_forgery with: :exception, unless: :json_request




  private

  def json_request
    request.format.json?
  end

  def render_template!(template, **attr)
    path = if template.is_a?(Symbol)
             folder, partial =  template.to_s, template.to_s

             "#{params[:controller]}/#{folder}/#{partial}"
           else
             template
           end

    render template: path, locals: attr
  end
end
