class Admin::DealsController < Admin::BaseController
  before_action :set_deal, only: [:edit, :update, :destroy]

  def edit
    render_template!(:edit, deal: @deal)
  end

  def update
    if @deal.update(deals_params)
      redirect_to admin_merchant_path(params[:deal][:merchant_id]), notice: 'Deal was successfully updated.'
    else
      render_template!(:edit, decor_merchant: @merchant.decorate)
    end
  end


  def create
    deal = Deal.new(deals_params)
    deal.items_left = deals_params[:items_total]

    if deal.save
      redirect_to admin_merchant_path(params[:deal][:merchant_id]), notice: 'Deal was successfully created.'
    else
      render_template!(:show)
    end
  end


  def destroy
    @deal.destroy
    redirect_to admin_merchant_path(params[:merchant_id]), notice: 'Merchant was successfully destroyed.'
  end



  private

  def set_deal
    @deal = Deal.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def deals_params
    params.require(:deal).permit(:name, :image, :merchant_id, :tag, :items_total, :items_left, :quality, :category_id)
  end
end
