class Admin::MerchantsController < Admin::BaseController
  before_action :set_merchant, only: [:show, :edit, :update, :destroy]

  def index
    merchants_collection = Merchant.order(id: :desc)
                                    .page(params[:page])
                                    .per(20)
    render_template!(:landing, decor_merchants: MerchantDecorator.decorate_collection(merchants_collection))
  end

  def show
    merchant_deals = @merchant.deals
                              .includes(:category)
                              .order(created_at: :desc)

    render_template!(:show, merchant: @merchant, deals: merchant_deals)
  end

  def new
    render_template!(:new, new_merchant: Merchant.new)
  end

  def edit
    render_template!(:edit, decor_merchant: @merchant.decorate)
  end

  def create
    new_merchant = Merchant.new(merchants_params)

    if new_merchant.save
      redirect_to admin_merchants_path, notice: 'Merchant was successfully created.'
    else
      render_template!(:new, new_merchant: new_merchant)
    end
  end

  def update
    if @merchant.update(merchants_params)
      redirect_to admin_merchants_path, notice: 'Merchant was successfully updated.'
    else
      render_template!(:edit, decor_merchant: @merchant.decorate)
    end
  end

  def destroy
    @merchant.destroy
    redirect_to admin_merchants_path, notice: 'Merchant was successfully destroyed.'
  end



  private

  def set_merchant
    @merchant = Merchant.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def merchants_params
    params.require(:merchant).permit(:email, :store_name, :store_email, :phone, :address, :logo, :lat, :lng)
  end
end
