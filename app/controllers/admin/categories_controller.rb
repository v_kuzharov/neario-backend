class Admin::CategoriesController < Admin::BaseController
  before_action :set_category, only: [:edit, :update, :destroy]

  def index
    merchants_collection = Category.order(id: :desc)
                                   .page(params[:page])
                                   .per(20)
    render_template!(:landing, decor_categories: CategoryDecorator.decorate_collection(merchants_collection))
  end

  def new
    render_template!(:new, new_category: Category.new)
  end

  def edit
    render_template!(:edit, decor_category: @category.decorate)
  end

  def create
    new_category = Category.new(category_params)

    if new_category.save
      redirect_to admin_categories_path, notice: 'Category was successfully created.'
    else
      render_template!(:new, new_category: new_category)
    end
  end

  def update
    if @category.update(category_params)
      redirect_to admin_categories_path, notice: 'Category was successfully updated.'
    else
      render_template!(:edit, decor_category: @category.decorate)
    end
  end

  def destroy
    @category.destroy
    redirect_to admin_categories_path, notice: 'Category was successfully destroyed.'
  end



  private

  def set_category
    @category = Category.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def category_params
    params.require(:category).permit(:name, :image)
  end
end