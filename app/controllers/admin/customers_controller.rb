class Admin::CustomersController < Admin::BaseController
  before_action :set_customer, only: [:edit, :update, :destroy]

  def index
    customers_collection = Customer.order(id: :desc)
                                   .page(params[:page])
                                   .per(20)
    render_template!(:landing, decor_customers: CustomerDecorator.decorate_collection(customers_collection))
  end

  def new
    render_template!(:new, new_customer: Customer.new)
  end

  def edit
    render_template!(:edit, decor_customer: @customer.decorate)
  end

  def update
    if @customer.update(customers_params)
      redirect_to admin_customers_path, notice: 'Customer user was successfully updated.'
    else
      render_template!(:edit, decor_customer: @customer.decorate)
    end
  end

  def destroy
    @customer.destroy
    redirect_to admin_customers_path, notice: 'Customer user was successfully destroyed.'
  end



  private

  def set_customer
    @customer = Customer.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def customers_params
    params.require(:customer).permit(:first_name, :first_name, :email, :mailing)
  end
end