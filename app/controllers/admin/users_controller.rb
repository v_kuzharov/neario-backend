class Admin::UsersController < ApplicationController
  before_action :set_admin_user, only: [:edit, :update, :destroy]

  # GET /admin/users
  def index
    admins_collection = Admin.order(id: :asc).page(params[:page]).per(30)
    render_template!(:landing, decor_admins: AdminDecorator.decorate_collection(admins_collection))
  end

  # GET /admin/users/new
  def new
    render_template!(:new, new_admin: Admin.new)
  end

  # GET /admin/users/1/edit
  def edit
    render_template!(:edit, decor_admin: @admin_user.decorate)
  end

  # POST /admin/users
  def create
    new_admin = Admin.new(admin_params)

    if new_admin.save
      redirect_to admin_users_path, notice: 'Admin was successfully created.'
    else
      render_template!(:new, new_admin: new_admin)
    end
  end

  # PATCH/PUT /admin/users/1
  def update
    if @admin_user.update(admin_params)
      redirect_to admin_users_path, notice: 'Admin user was successfully updated.'
    else
      render_template!(:edit, decorated_admin: @admin_user.decorate)
    end
  end

  # DELETE /admin/users/1
  def destroy
    @admin_user.destroy
    redirect_to admin_users_url, notice: 'Admin user was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_admin_user
    @admin_user ||= Admin.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def admin_params
    params.require(:admin).permit(:email, :password, :password_confirmation)
  end
end
