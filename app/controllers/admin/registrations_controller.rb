class Admin::RegistrationsController < Devise::RegistrationsController

  # GET /resource/sign_up
  def new
    redirect_to new_admin_session_path
  end
end