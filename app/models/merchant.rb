class Merchant < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # Added :jwt strategy, see devise.rb
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Uploaders
  mount_uploader :logo, MerchantLogoUploader

  # Relations
  has_many :deals
  has_many :purchases
  has_many :customers, through: :purchases
end
