class Deal < ApplicationRecord

  # Uploaders
  mount_uploader :image, DealImageUploader

  # Relations
  belongs_to :category
  belongs_to :merchant
end
