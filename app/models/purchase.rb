class Purchase < ApplicationRecord

  # Relations
  belongs_to :customer
  belongs_to :merchant

  enum status: { claim: 0, redeem: 1, expired: 2 }
end
