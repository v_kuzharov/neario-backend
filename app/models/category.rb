class Category < ApplicationRecord

  # Uploaders
  mount_uploader :image, CategoryImageUploader

  # Relations
  has_many :deals, dependent: :destroy
end
