class ErrorsSerializer < ActiveModel::Serializer
  attributes :details

  def details
    instance_options[:d]
  end
end