// import 'select2'
// import 'select2/dist/css/select2.min.css'
//
// document.addEventListener('DOMContentLoaded', () => {
//     $('#new-tag').select2({
//         tags: true,
//         width: '100%',
//         tokenSeparators: [','],
//         minimumInputLength: 2,
//         placeholder: "Select or new tag",
//         allowClear: true,
//         ajax: {
//             url: '/admin/tags',
//             dataType: 'json',
//             type: 'GET',
//             data: function(params) {
//                 return {
//                     name: params.term,
//                     tags_chosen: $(this).val(),
//                     taggable_type: $(this).data('taggable-type'),
//                     context: $(this).data('context'),
//                     page: params.page
//                 };
//             },
//             processResults: function(data, params) {
//                 params.page = params.page || 1;
//                 return {
//                     results: $.map(data, function(item) {
//                         return {
//                             text: item.name,
//                             id: item.name
//                         };
//                     })
//                 };
//             },
//             cache: true
//         }
//     });
// });