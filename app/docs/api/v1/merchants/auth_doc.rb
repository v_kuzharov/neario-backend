module Api
  module V1
    class Merchants::AuthDoc < ApplicationDoc
      resource_description do
        description 'Merchants Authentication'
      end

      doc_for :login do
        api :POST, '/v1/m/auth/login', 'Merchants Login'

        param :merchant, Hash, desc: 'Merchant credentials', required: true do
          param :email, String, desc: 'Merchant email for login', required: true
          param :password, String, desc: 'Password for login', required: true
        end

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json"
            }

          === Params
            "merchant":{
                "email": "vk@test.com",
                "password": "123123"
              }

          == Response
                "data": {
                    "auth_token": "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwidHlwZSI6ImN1c3RvbWVyIiwianRpIjoiNDRiZDg1MjExMWUxOGEwMmVmOWZkOTczM2U5YzVmZGE3M2U1N2IwYSIsImV4cCI6MTU1NjE3MDg5NH0.vs4HnbAPPgMPijSU1p3XkauPRyJQrGOg0xGdjOjR_ac",
                    "merchant": {
                        "store_name": "vkStoreName",
                        "store_email": "vk.store@merchant.com",
                        "email": "vk@merchant.com",
                        "phone": "123456789",
                        "address": "t",
                        "lat": 47.216656,
                        "lng": 38.911721
                    }
                }
        EOS

        error code: 400, desc: 'Bad Request'
        example "
          {
            'errors': {
              'details': {
                  'email': [
                      'can't be blank'
                  ]
              }
            }
          }
        "

        error code: 400, desc: 'Bad Request'
        example "
          {
            'errors': {
              'details': 'details': 'Email: vk@merchant.com is doesnt exist!'
              }
            }
          }
        "
      end

      doc_for :register do
        api :POST, '/v1/m/auth/register', 'Merchant Register'

        param :merchant, Hash, desc: 'Merchant credentials', required: true do
          param :email, String, desc: 'Merchant email for sign up', required: true
          param :store_name, String, desc: 'Merchant Store for sign up', required: true
          param :store_email, String, desc: 'Merchant Store email for sign up', required: true
          param :password, String, desc: 'Password for sign up', required: true
          param :password_confirmation, String, desc: 'Password Confirmation', required: true
          param :phone, String, desc: 'Merchant Phone'
          param :lat, String, desc: 'Merchant latitude'
          param :lng, String, desc: 'Merchant longitude'
        end

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json"
            }

          === Params
            {
              "merchant":{
                "email": "vk@merchant.com",
                "store_name": "vkStoreName",
                "store_email": "vk.store@merchant.com",
                "password": "123456",
                "password_confirmation": "123456",
                "phone": 123456789,
                "address": "address goes here",
                "lat": 47.216656,
                "lng": 38.911721
                
              }
            }

          == Response
            "data": {
                "auth_token": "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwidHlwZSI6ImN1c3RvbWVyIiwianRpIjoiOGUyNjc2MzI1OTY2M2NhNGNkNjY5YmZlMzA2OTFlZDJkZTQ4MmRmMiIsImV4cCI6MTU1NjE3MTI3OX0.KdPsfggL7R4ROJZaeFhJ1sm-4_iFUZvgsAigi5jW8v4",
                "merchant": {
                    "store_name": "vkStoreName",
                    "store_email": "vk.store@merchant.com",
                    "email": "vk@merchant.com",
                    "phone": "123456789",
                    "address": "t",
                    "lat": 47.216656,
                    "lng": 38.911721
                }
            }
        EOS

        error code: 400, desc: 'Email has already been taken'
        example "
          {
              'errors': {
                  'details': {
                      'email': [
                          'has already been taken'
                      ]
                  }
              }
          }
        "
      end


      doc_for :logout do
        api :DELETE, '/v1/m/auth/logout', 'Merchant Logout'

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json",
              "Auth": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxNCwianRpIjoiMWYzNGZjNjRiNzRiMGRlOTZjZWYiLCJleHAiOjE1MTMzNjg2OTR9.A1Fc-LaC1sp7y0TyHIJcBBWRjVbWU4FEWYDvwP7jbMI"
            }

          === Params
            {}

          == Response

            {
                "message": "Sign out successfully!"
            }
        EOS
      end
    end
  end
end