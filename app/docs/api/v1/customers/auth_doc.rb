module Api
  module V1
    class Customers::AuthDoc < ApplicationDoc
      resource_description do
        description 'Customers Authentication'
      end

      doc_for :login do
        api :POST, '/v1/c/auth/login', 'Customer Login'

        param :customer, Hash, desc: 'Customer credentials', required: true do
          param :email, String, desc: 'Customer email for login', required: true
          param :password, String, desc: 'Password for login', required: true
        end

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json"
            }

          === Params
            "customer":{
                "email": "vk@test.com",
                "password": "123123"
              }

          == Response
                "data": {
                    "auth_token": "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwidHlwZSI6ImN1c3RvbWVyIiwianRpIjoiNDRiZDg1MjExMWUxOGEwMmVmOWZkOTczM2U5YzVmZGE3M2U1N2IwYSIsImV4cCI6MTU1NjE3MDg5NH0.vs4HnbAPPgMPijSU1p3XkauPRyJQrGOg0xGdjOjR_ac",
                    "customer": {
                        "first_name": "vkName",
                        "last_name": "vkName",
                        "email": "vk@customer.com",
                        "mailing": true
                    }
                }
        EOS

        error code: 400, desc: 'Bad Request'
        example "
          {
            'errors': {
              'details': {
                  'email': [
                      'can't be blank'
                  ]
              }
            }
          }
        "

        error code: 400, desc: 'Bad Request'
        example "
          {
            'errors': {
              'details': 'details': 'Email: vk@customer.com is doesnt exist!'
              }
            }
          }
        "
      end

      doc_for :register do
        api :POST, '/v1/c/auth/register', 'Customer Register'

        param :customer, Hash, desc: 'Customer credentials', required: true do
          param :first_name, String, desc: 'Customer first name for sign up', required: true
          param :last_name, String, desc: 'Customer last name for sign up', required: true
          param :email, String, desc: 'Customer email for sign up', required: true
          param :password, String, desc: 'Password for sign up', required: true
          param :password_confirmation, String, desc: 'Password Confirmation', required: true
          param :mailing, [true, false], desc: 'Subscribe for mailing list'
          param :referral_id, String, desc: 'Referral code'
        end

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json"
            }

          === Params
            {
              "customer":{
                "email": "vk@customer.com",
                "first_name": "vkName",
                "last_name": "vkName",
                "password": "123456",
                "password_confirmation": "123456",
                "mailing": true,
                "referral_id": "qwerty"
              }
            }

          == Response
            "data": {
                "auth_token": "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwidHlwZSI6ImN1c3RvbWVyIiwianRpIjoiOGUyNjc2MzI1OTY2M2NhNGNkNjY5YmZlMzA2OTFlZDJkZTQ4MmRmMiIsImV4cCI6MTU1NjE3MTI3OX0.KdPsfggL7R4ROJZaeFhJ1sm-4_iFUZvgsAigi5jW8v4",
                "user": {
                    "first_name": "vkName",
                    "last_name": "vkName",
                    "email": "vk@customer2.com",
                    "mailing": true
                }
            }
        EOS

        error code: 400, desc: 'Email has already been taken'
        example "
          {
              'errors': {
                  'details': {
                      'email': [
                          'has already been taken'
                      ]
                  }
              }
          }
        "
      end


      doc_for :logout do
        api :DELETE, '/v1/c/auth/logout', 'Customer Logout'

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json",
              "Auth": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxNCwianRpIjoiMWYzNGZjNjRiNzRiMGRlOTZjZWYiLCJleHAiOjE1MTMzNjg2OTR9.A1Fc-LaC1sp7y0TyHIJcBBWRjVbWU4FEWYDvwP7jbMI"
            }

          === Params
            {}

          == Response

            {
                "message": "Sign out successfully!"
            }
        EOS
      end
    end
  end
end