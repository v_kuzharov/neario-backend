module Api
  module V1
    class Customers::PurchasesDoc < ApplicationDoc
      resource_description do
        description 'Purchase'
      end

      doc_for :create do
        api :POST, '/v1/c/purchases', 'Create purchase'

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json",
              "Auth": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkI"
            }

          === Params
            "purchase":{
              "deal_id": 3,
              "merchant_id": 2
            }

          == Response
              "data": {
                  "purchase": {
                    "id": 83,
                    "status": "claim",
                    "time_left": null,
                    "merchant": {
                        "id": 2,
                        "store_name": "vkStoreName",
                        "store_email": "vk.store@merchant.com",
                        "email": "vk@merchant.com",
                        "phone": "123456789",
                        "address": "185 Berry st",
                        "lat": 47.216656,
                        "lng": 38.911721
                    }
                }
              }
        EOS

        error code: 422, desc: 'Deal is already claimed by current customer'
        example "
          {
              'errors': {
                  'details': 'Deal with id: 3 is already claimed by vk@customer223.com!'
              }
          }
        "

        error code: 404, desc: 'Deal not found'
        example "
          {
              'errors': {
                  'details': 'Couldn't find Deal with id: 33'
              }
          }
        "

        error code: 404, desc: 'Merchant not found'
        example "
          {
              'errors': {
                  'details': 'Couldn't find Merchant with id: 43'
              }
          }
        "

        error code: 400, desc: 'Deal items is zero'
        example "
          {
              'errors': {
                  'details': 'Items for deal with id: 3 is 0!'
              }
          }
        "
      end

      doc_for :history do
        api :GET, '/v1/c/purchases/history', 'Current Customer purchases history list'


        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json",
              "Auth": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkI"
            }

          === Params
            {}

          == Response
            "data": {
                "purchases": [
                    {
                        "id": 118,
                        "status": "claim",
                        "time_left": null,
                        "merchant": {
                            "id": 2,
                            "store_name": "vkStoreName",
                            "store_email": "vk.store@merchant.com",
                            "email": "vk@merchant.com",
                            "phone": "123456789",
                            "address": "185 Berry st",
                            "lat": 47.216656,
                            "lng": 38.911721
                        }
                    }
                ]
            }
        EOS
      end
    end
  end
end