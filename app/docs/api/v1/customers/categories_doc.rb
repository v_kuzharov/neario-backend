module Api
  module V1
    class Customers::CategoriesDoc < ApplicationDoc
      resource_description do
        description 'Categories'
      end

      doc_for :index do
        api :GET, '/v1/c/categories', 'Categories List'

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json"
            }

          === Params
            "customer":{}

          == Response
                "data": {
                    "categories": [
                        {
                            "id": 3,
                            "name": "qwerty123",
                            "logo": "http://images.clipartpanda.com/category-clipart-category_subcategory_puzzle.png"
                        },
                        {
                            "id": 4,
                            "name": "qwerty",
                            "logo": "https://res.cloudinary.com/maven-dev/image/upload/v1556615320/vjfu2kgydohgmgiienx3.png"
                        }
                    ]
                }
        EOS
      end

      doc_for :category_deals do
        api :GET, '/v1/c/categories/:category_id/deals', 'Deals list by Category id'

        param :category_id, Integer, desc: 'Category Id', required: true

        description <<-EOS
          == Request

          === Headers
            {
              "Content-Type": "application/json"
            }

          === Params
            {}

          == Response
            "data": {
                "deals": [
                    {
                        "id": 2,
                        "name": "$212 Chips & Guacamole",
                        "merchant": {
                            "name": "vkStoreName",
                            "address": "185 Berry st",
                            "phone": "123456789",
                            "lat": 47.216656,
                            "lng": 38.911721
                        },
                        "image": "https://spiritedgifts.com/media/catalog/product/cache/1/image/1800x/040ec09b1e35df139433887a97daa66f/p/a/patron-gift-basket.png",
                        "items_total": 11,
                        "items_left": 0,
                        "quality": "Excellent",
                        "category_id": 4
                    }
                ]
            }
        EOS

        error code: 404, desc: 'Invalid category id'
        example "
          {
              'errors': {
                  'details': 'Couldnt find Category with id=6'
              }
          }
        "
      end
    end
  end
end