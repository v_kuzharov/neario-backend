class DealImageUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  # Choose what kind of storage to use for this uploader:
  # storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  # def store_dir
  #   "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  # end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url(*args)
    'https://spiritedgifts.com/media/catalog/product/cache/1/image/1800x/040ec09b1e35df139433887a97daa66f/p/a/patron-gift-basket.png'
  end

  # Create different versions of your uploaded files:
  version :standard do
    process resize_to_fill: [200, 200, :north]
  end

  # version :small do
  #   resize_to_fit(100, 100)
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_whitelist
    %w(jpg jpeg png)
  end
end
