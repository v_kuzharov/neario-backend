class Merchant::Register < BaseInteractor

  # Expected parameters
  delegate :email, :store_name, :store_email, :password, :password_confirmation, :phone, :address, :lat, :lng, to: :context

  # Validate incoming parameters
  validates :store_name,
            presence: true,
            length: 3..20

  validates :store_email,
            presence: true,
            format: { with: EMAIL_REGEXP, message: "it's not an email!" }

  validates :email,
            presence: true,
            format: { with: EMAIL_REGEXP, message: "it's not an email!" },
            uniqueness: { case_sensitive: false, model: Merchant }

  validates :password,
            confirmation: true,
            length: 6..20

  validates :phone,
            presence: true # add more validation in future

  validates :address,
            presence: true

  validates :lat,
            presence: true,
            numericality: true

  validates :lng,
            presence: true,
            numericality: true



  def call
    merchant = Merchant.create(merchant_attributes)

    context.merchant = merchant
    context.token = generate_token_for(merchant)
  end



  private

  def merchant_attributes
    {
        jti: generate_jti,
        store_name: store_name,
        store_email: store_email,
        email: email,
        password: password,
        password_confirmation: password_confirmation,
        phone: phone,
        address: address,
        lat: lat,
        lng: lng
    }
  end
end