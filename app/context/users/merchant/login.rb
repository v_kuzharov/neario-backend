class Merchant::Login < BaseInteractor

  # Expected parameters
  delegate :email, :password, to: :context

  # Validate incoming parameters
  validates :email,
            presence: true,
            format: { with: EMAIL_REGEXP, message: "it's not an email!" }

  validates :password,
            length: 6..20


  def call
    detect_merchant!
    authenticate_merchant!
    update_jti!

    context.merchant = @merchant
    context.token = generate_token_for(@merchant)
  end



  private

  def detect_merchant!
     @merchant = Merchant.find_by_email(email)
    context.fail!(errors: "Email: #{email} is doesn't exist!") if @merchant.blank?
  end

  def authenticate_merchant!
    context.fail!(errors: 'Invalid password!') unless @merchant.valid_password?(password)
  end

  def update_jti!
    @merchant.update!(jti: generate_jti)
  end
end