class CustomerSerializer < ActiveModel::Serializer
  attributes :first_name, :last_name, :email, :mailing
end