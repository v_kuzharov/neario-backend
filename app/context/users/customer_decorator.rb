class CustomerDecorator < ApplicationDecorator
  delegate_all

  def full_name
    [object.first_name, object.last_name].join(' ')
  end

  def is_maling?
    "fa fa-#{object.mailing? ? 'check text-success' : 'close text-danger' }"
  end
end