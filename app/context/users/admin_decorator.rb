class AdminDecorator < ApplicationDecorator
  delegate_all

  def current_admin?
    current_admin.id == object.id
  end
end