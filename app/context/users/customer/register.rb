class Customer::Register < BaseInteractor

  # Expected parameters
  delegate :first_name, :last_name, :email, :password, :password_confirmation, :mailing, to: :context

  # Validate incoming parameters
  validates :first_name,
            presence: true,
            length: 3..20

  validates :last_name,
            presence: true,
            length: 3..20

  validates :email,
            presence: true,
            uniqueness: { case_sensitive: false, model: Customer },
            format: { with: EMAIL_REGEXP, message: "it's not an email!" }

  validates :password,
            confirmation: true,
            length: 6..20

  validates :mailing,
            inclusion: { in: [ true, false ] }



  def call
    customer = Customer.create(customer_attributes)

    context.customer = customer
    context.token = generate_token_for(customer)
  end



  private

  def customer_attributes
    {
        jti: generate_jti,
        first_name: first_name,
        last_name: last_name,
        email: email,
        password: password,
        password_confirmation: password_confirmation,
        mailing: mailing,
        referral_id: generate_referral_id,
    }
  end
end