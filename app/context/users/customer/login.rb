class Customer::Login < BaseInteractor

  # Expected parameters
  delegate :email, :password, to: :context

  # Validate incoming parameters
  validates :email,
            presence: true,
            format: { with: EMAIL_REGEXP, message: "it's not an email!" }

  validates :password,
            length: 6..20


  def call
    detect_customer!
    authenticate_customer!
    update_jti!

    context.customer = @customer
    context.token = generate_token_for(@customer)
  end



  private

  def detect_customer!
    @customer = Customer.find_by_email(email)
    context.fail!(errors: "Email: #{email} not found!") if @customer.blank?
  end

  def authenticate_customer!
    context.fail!(errors: 'Invalid password!') unless @customer.valid_password?(password)
  end

  def update_jti!
    @customer.update!(jti: generate_jti)
  end
end