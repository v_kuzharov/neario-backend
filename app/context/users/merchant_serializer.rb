class MerchantSerializer < ActiveModel::Serializer
  attributes :id, :store_name, :store_email, :email, :phone, :address, :lat, :lng
end