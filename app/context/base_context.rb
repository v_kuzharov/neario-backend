class BaseContext
  include JWTWrapper
  include ActiveModel::Validations

  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/


  private

  def generate_jti
    SecureRandom.hex(20)
  end

  def generate_referral_id
    SecureRandom.hex(6)
  end


  protected

  def generate_token_for(resource)
    JWTWrapper.encode({ id: resource.id, type: resource.class.to_s.downcase, jti: resource.jti })
  end
end