class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :logo

  def logo
    object.image.url
  end
end