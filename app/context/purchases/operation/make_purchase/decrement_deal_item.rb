class Operation::MakePurchase::DecrementDealItem < BaseInteractor

  # Expected parameters
  delegate :deal_object, to: :context


  def call
    context.fail!(errors: "There is no items left for deal with id: #{deal_object.id}!", error_status: :bad_request) if deal_object.items_left.zero?

    deal_object.decrement!(:items_left) if deal_object.items_left > 0
  end
end