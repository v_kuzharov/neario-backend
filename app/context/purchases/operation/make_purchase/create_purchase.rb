class Operation::MakePurchase::CreatePurchase < BaseInteractor

  # Expected parameters
  delegate :customer, :deal_object, :merchant_object, to: :context


  def call
    purchase = Purchase.create(purchase_attributes)
    context.purchase = purchase
  end



  private

  def purchase_attributes
    {
        customer_id: customer.id,
        merchant_id: merchant_object.id,
        deal_id: deal_object.id
    }
  end
end