class Operation::MakePurchase < BaseOrganizer

  # Expected parameters
  delegate :customer, :deal_id, :merchant_id, to: :context


  # Validate parameters and objects
  validates :merchant_id,
            presence: true

  validates :deal_id,
            presence: true

  validate :deal_exist?

  validate :merchant_exist?

  validate :deal_already_purchased? # Customer can claim only one deal item



  # Run interactors in right order
  organize DecrementDealItem, CreatePurchase



  private

  def deal_exist?
    deal = Deal.find_by_id(deal_id)

    if deal.present?
      context.deal_object = deal
    else
      context.fail!(errors: "Couldn't find Deal with id: #{deal_id}", error_status: :not_found)
    end
  end

  def merchant_exist?
    merchant = Merchant.find_by_id merchant_id

    if merchant.present?
      context.merchant_object = merchant
    else
      context.fail!(errors: "Couldn't find Merchant with id: #{merchant_id}", error_status: :not_found)
    end
  end

  def deal_already_purchased?
    if Purchase.exists?(customer_id: customer.id, deal_id: deal_id)
      context.fail!(errors: "Deal with id: #{deal_id} is already claimed by #{customer.email}!", error_status: :unprocessable_entity)
    end
  end
end