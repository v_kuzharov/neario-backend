class PurchaseSerializer < ActiveModel::Serializer
  attributes :id, :status

  belongs_to :merchant, serializer: MerchantSerializer
end