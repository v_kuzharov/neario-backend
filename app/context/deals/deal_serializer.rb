class DealSerializer < ActiveModel::Serializer
  attributes :id, :name, :image, :items_total, :items_left, :quality, :category_id

  belongs_to :merchant, serializer: MerchantSerializer

  def image
    object.image.url
  end
end