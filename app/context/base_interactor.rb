class BaseInteractor < BaseContext
  include Interactor

  def self.inherited(subclass)
    subclass.class_eval do
      # "before" hook is a gem "interactor" specific. See documentation.
      before do
        context.fail!(errors: errors) unless valid? # runs every validation
      end
    end
  end
end