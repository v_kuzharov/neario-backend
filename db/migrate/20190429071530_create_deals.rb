class CreateDeals < ActiveRecord::Migration[5.2]
  def change
    create_table :deals do |t|
      t.references :merchant, index: true
      t.string :name,         null: false, default: ''
      t.string :image,                     default: ''
      t.integer :items_total, null: false, default: 0
      t.integer :items_left,  null: false, default: 0
      t.string :quality,      null: false, default: ''


      t.timestamps
    end
  end
end
