class RemoveUniqueIndexFromCustomers < ActiveRecord::Migration[5.2]
  def change
    remove_index :customers, :facebook_id
  end
end
