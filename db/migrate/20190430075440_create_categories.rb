class CreateCategories < ActiveRecord::Migration[5.2]
  def up
    create_table :categories do |t|
      t.string :name,  null: false
      t.string :image, default: ''

      t.timestamps
    end

    add_column :deals, :category_id, :integer, index: true
    add_index :categories, :name, unique: true
  end


  def down
    drop_table :categories
    remove_column :deals, :category_id
  end
end
