class CreatePurchases < ActiveRecord::Migration[5.2]
  def change
    create_table :purchases do |t|
      t.references :customer, foreign_key: true, index: true
      t.references :merchant, foreign_key: true, index: true
      t.integer :deal_id
      t.integer :status,      null: false,       index: true, default: 0

      t.timestamps
    end
  end
end
