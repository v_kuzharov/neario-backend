# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_06_094121) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "name", null: false
    t.string "image", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_categories_on_name", unique: true
  end

  create_table "customers", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.boolean "mailing", default: false
    t.string "jti", default: ""
    t.string "referral_id", default: ""
    t.string "facebook_id", default: ""
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_customers_on_email", unique: true
    t.index ["jti"], name: "index_customers_on_jti", unique: true
    t.index ["referral_id"], name: "index_customers_on_referral_id", unique: true
    t.index ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true
  end

  create_table "deals", force: :cascade do |t|
    t.bigint "merchant_id"
    t.string "name", default: "", null: false
    t.string "image", default: ""
    t.integer "items_total", default: 0, null: false
    t.integer "items_left", default: 0, null: false
    t.string "quality", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category_id"
    t.index ["merchant_id"], name: "index_deals_on_merchant_id"
  end

  create_table "merchants", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "store_name", default: "", null: false
    t.string "store_email", default: "", null: false
    t.string "address", default: "", null: false
    t.string "phone", default: ""
    t.string "logo", default: ""
    t.float "lat", default: 0.0
    t.float "lng", default: 0.0
    t.string "jti", default: ""
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_merchants_on_email", unique: true
    t.index ["jti"], name: "index_merchants_on_jti", unique: true
    t.index ["lat"], name: "index_merchants_on_lat"
    t.index ["lng"], name: "index_merchants_on_lng"
    t.index ["reset_password_token"], name: "index_merchants_on_reset_password_token", unique: true
    t.index ["store_email"], name: "index_merchants_on_store_email", unique: true
    t.index ["store_name"], name: "index_merchants_on_store_name"
  end

  create_table "purchases", force: :cascade do |t|
    t.bigint "customer_id"
    t.bigint "merchant_id"
    t.integer "deal_id"
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_purchases_on_customer_id"
    t.index ["merchant_id"], name: "index_purchases_on_merchant_id"
    t.index ["status"], name: "index_purchases_on_status"
  end

  add_foreign_key "purchases", "customers"
  add_foreign_key "purchases", "merchants"
end
