require_relative 'boot'

require 'rails/all'
require_relative 'middleware/devise_basic_auth_fix'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module NearioBackend
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.middleware.use DeviseBasicAuthFix
    config.autoload_paths += Dir.glob("#{config.root}/app/context/*")

    config.generators do |g|
      g.assets false
      g.helper false
      g.test_framework nil
    end
  end
end
