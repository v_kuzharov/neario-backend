##
# Rack middleware for removing basic auth headers from requests
# see http://github.com/plataformatec/devise/issues/issue/178 for an explanation of why and when this is needed...
# Find in application.rb
# === Examples:
#
#  use DeviseBasicAuthFix
#
#

class DeviseBasicAuthFix
  def initialize(app)
    @app = app
  end

  def call(env)
    env['HTTP_AUTHORIZATION'] = nil
    @app.call(env)
  end
end