module Devise
  module Strategies
    class JsonWebToken < Base
      def valid?
        request.headers['Auth'].present?
      end

      def authenticate!
        return fail! unless claims
        return fail! if user.nil?

        env['devise.skip_trackable'] = true
        success! user
      end

      def store?
        false
      end

      def user
        @user ||= entity.find_by_jti @claims['jti']
      end

      protected

      def claims
        strategy, token = request.headers['Auth'].split(' ')

        return nil if (strategy || '').downcase != 'bearer'
        @claims ||= JWTWrapper.decode(token)
      rescue
        nil
      end

      def entity
        @claims['type'].classify.constantize
      end
    end
  end
end