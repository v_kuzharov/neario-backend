Apipie.configure do |config|
  config.app_name                = 'Neario'
  config.api_base_url            = '/api'
  config.doc_base_url            = '/apidocs'
  config.translate               = false
  config.namespaced_resources    = true
  config.default_locale          = nil
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/**/*.rb"
end
