require 'sidekiq/web'

Rails.application.routes.draw do

  root 'admin/dashboard#index'

  authenticated :admin, lambda { |u| u.email }  do
    mount Sidekiq::Web => '/sidekiq'
    apipie
  end

  devise_for :admins,
             skip: [:registrations, :passwords],
             controllers: {
                 registrations: 'admin/registrations',
                 sessions:      'admin/sessions'
             }

  devise_for :merchants,
             skip: [:registrations, :sessions, :passwords]

  devise_for :customers,
             skip: [:registrations, :sessions, :passwords]




  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do

      namespace :customers, path: '/c' do
        resources :auth, only: [] do
          collection do
            post :register, action: :register
            post :login, action: :login
            delete :logout, action: :logout
          end
        end

        resources :categories, only: :index do
          get :deals, action: :category_deals
        end

        resources :purchases, only: :create do
          collection do
            get :history, action: :history
          end
        end
      end

      namespace :merchants, path: '/m' do
        resources :auth, only: [] do
          collection do
            post :login, action: :login
            post :register, action: :register
            delete :logout, action: :logout
          end
        end
      end

    end
  end

  namespace :admin do
    resources :dashboard, only: :index
    resources :categories
    resources :users
    resources :customers
    resources :merchants
    resources :deals

  end
end
